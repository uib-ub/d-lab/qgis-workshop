# GIS-workshop 2022

## Installing QGIS
 We recommend using QGIS version 3.12 or newer.

 ### Installing on a windows computer managed by UiB
1. Search for "Software center" in the start menu and launch it.
2. Search "qgis" in the Software Center and click on the search result
3. Click "install" and wait for qgis to be installed on your system.

 ### Installing on a private computer

Installer:
https://qgis.org/en/site/forusers/index.html

1. Click "Get the installer"
2. Expand the section for you operating system
3. Choose version 3.22 (Windows users: standalone installer, not Network installer)

## Lesson materials
[Download lesson materials](QGIS_DLAB.zip)
